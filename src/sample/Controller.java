package sample;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Controller - allows user to use program
 */
public class Controller extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.getIcons().add(new Image("file:out/production/ourWeatherClient/OurWeatherSmall.png")); //app icon
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("OurWeather Viewer");
        primaryStage.setScene(new Scene(root));
        //start
        primaryStage.show();
    }
    @FXML
    private CategoryAxis x;
    @FXML
    private NumberAxis y;
    @FXML
    private LineChart<?, ?> LineChart;
    @FXML
    private Label statOne;
    @FXML
    private Label statTwo;
    @FXML
    private Label statThree;
    @FXML
    private Label statFour;
    @FXML
    private Label lastTime;
    @FXML
    private ChoiceBox chBox;
    private long startTime = 0;
    private int category = 1;
    private String[] catLabels = {"Time","Temperature (C)","Light (Lux)","Humidity (PPM)","Pressure (Bar)"};
    private LocalDateTime startDate = null;

    public void updateData(){
        startTime = System.nanoTime();
        System.out.println("Updating data!");
        //statOne.setText("Temperature: 34.5 C");
        //statTwo.setText("Humidity: 532 PPM"); //lookup units
        //statThree.setText("Air pressure: 1.23 Bar");
        //statFour.setText("Light: 42000 Lux");
        TCPClient database = new TCPClient();
        System.out.print("\n");
        int amountOfColumns = 5;
        List<String> myList = new ArrayList<String>(Arrays.asList(database.msg("requestAll").replaceAll("\\[", "").replaceAll("]", "").split(", ")));
        int rows = myList.size()/amountOfColumns-1;

        //Clear chart
        LineChart.getData().clear();
        LineChart.layout();
        //XYChart.Series series = new XYChart.Series();
        //series.setName("No of schools in an year");

        //series.getData().add(new XYChart.Data("1", 15));
        //series.getData().add(new XYChart.Data("2", 30));

        //Setting the data to Line chart
        //LineChart.getData().addAll(series);

        XYChart.Series temp = new XYChart.Series();
        XYChart.Series light = new XYChart.Series();
        XYChart.Series humidity = new XYChart.Series();
        XYChart.Series pressure = new XYChart.Series();

        temp.setName("Temperature (C)");
        light.setName("Light (Lux)");
        humidity.setName("Humidity (PPM)");
        pressure.setName("Pressure (Bar)");
        for (int i = 0; i < rows+1; i++) {
            if(startDate == null || convertTime(myList.get(i * amountOfColumns)).isAfter(startDate)){
                temp.getData().add(new XYChart.Data<>(myList.get(i * amountOfColumns), Float.parseFloat(myList.get(i * amountOfColumns + 3))));
                light.getData().add(new XYChart.Data<>(myList.get(i * amountOfColumns), Float.parseFloat(myList.get(i * amountOfColumns + 1))));
                humidity.getData().add(new XYChart.Data<>(myList.get(i * amountOfColumns), Float.parseFloat(myList.get(i * amountOfColumns + 4))));
                pressure.getData().add(new XYChart.Data<>(myList.get(i * amountOfColumns), Float.parseFloat(myList.get(i * amountOfColumns + 2))));
            }
        }
        y.setLabel(catLabels[category]);
        statOne.setText("   Temperature: "+ myList.get(rows * amountOfColumns + 3) +" C");
        statThree.setText("   Pressure: "+ myList.get(rows * amountOfColumns + 2) +" Bar");
        statTwo.setText("   Humidity: "+ myList.get(rows * amountOfColumns + 4) +" PPM");
        statFour.setText("   Light: "+ myList.get(rows * amountOfColumns + 1) +" Lux");
        switch (category){
            case 1:
                LineChart.getData().addAll(temp);
                statOne.setText("> Temperature: "+ myList.get(rows * amountOfColumns + 3) +" C");
                break;
            case 2:
                LineChart.getData().addAll(light);
                statFour.setText("> Light: "+ myList.get(rows * amountOfColumns + 1) +" Lux");
                break;
            case 3:
                LineChart.getData().addAll(humidity);
                statTwo.setText("> Humidity: "+ myList.get(rows * amountOfColumns + 4) +" PPM");
                break;
            case 4:
                LineChart.getData().addAll(pressure);
                statThree.setText("> Pressure: "+ myList.get(rows * amountOfColumns + 2) +" Bar");
                break;
            default:
                LineChart.getData().clear();
                System.out.print("Category out of bounds!");
                break;
        }

        database.close();
    }
    public LocalDateTime convertTime(String inputString){
        //2018-01-15 17:24:34
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        LocalDateTime dateTime = LocalDateTime.parse(inputString, formatter);
        //System.out.println(dateTime.format(formatter)); // not using toString
        return dateTime;
        /*
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss SSS");
            LocalDate date = LocalDate.parse(inputString, formatter);
            return date;
        } catch(Exception e) {
            System.out.println("Ërror occured when converting date!");
        }
        return null;*/
    }
    public LocalDateTime subtractDays(int minus){
        LocalDateTime time = LocalDateTime.now().minusDays(minus);
        return time;
    }
    public void initialize(){
        System.out.println("Initialising!");
        TCPClient database = new TCPClient();
        System.out.print(database.msg("requestAll") + "\n");
        chBox.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            String val = chBox.getValue().toString();
            System.out.println(val);
            if(val.equals("Last hour")){
                startDate = LocalDateTime.now().minusHours(1);
            } else if(val.equals("Last day")){
                startDate = subtractDays(1);
            } else if(val.equals("Last week")){
                startDate = subtractDays(7);
            } else if(val.equals("Last month")){
                startDate = LocalDateTime.now().minus(Period.ofMonths(1));
            } else if(val.equals("All data")){
                startDate = null;
            } else if(val.equals("")){
                startDate = null;
            }
            updateData();
        });
        //prepare chart
        database.close();
        updateData();
        startUpdater();
    }
    public void startUpdater(){
        //create new timer
        /*Timer timer = new Timer();
        class task extends TimerTask {
            public void run() {
                updateData();
            }
        }*/

        Timeline fiveSecondsWonder = new Timeline(new KeyFrame(Duration.millis(50), new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
                long timeLine = (System.nanoTime()-startTime)/1000000000;
                if(timeLine>5){ updateData();}
                lastTime.setText("   Updated " + timeLine + " seconds ago");
            }
        }));
        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
        fiveSecondsWonder.play();
    }
    public void nextButton(){
        if(category == 4) {
            category = 1;
        } else {
            category++;
        }
        updateData();
        System.out.println("NEXT");
    }
    public void previousButton(){
        if(category == 1) {
            category = 4;
        } else {
            category--;
        }
        updateData();
        System.out.println("PREVIOUS");
    }
    /**
     * Overrides default exit method
     */
    public void stop(){
        System.exit(2);
    }
    /**
     * Launches other functions
     *
     * @param args Arguments passed by program parameters (not used)
     */
    public void main(String[] args) {
        launch(args);
    }
}
