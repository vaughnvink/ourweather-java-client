package sample;

import java.io.*;
import java.net.*;

class TCPClient {

    private String serverIP = "localhost";
    private int port = 42069;

    private boolean connected = false;
    private Socket clientSocket = null;
    private DataOutputStream serverIn;
    private BufferedReader serverOut;
    TCPClient () {

        while(!connected){
            try {
                clientSocket = new Socket(serverIP, port);
                serverIn = new DataOutputStream(clientSocket.getOutputStream());
                serverOut = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                System.out.println("Connected to OurWeather dataserver on " + clientSocket.getInetAddress());
                connected = true;
            } catch (IOException e) {
                System.out.println("Server not found, retrying in 5 seconds!");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException c) {
                }
                //e.printStackTrace();
            }
        }


    }
    TCPClient (String host, int port) {
        this.serverIP = host;
        this.port = port;
        try {
            clientSocket = new Socket(serverIP, port);
            serverIn = new DataOutputStream(clientSocket.getOutputStream());
            serverOut = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            System.out.println("Server not found!");
            //e.printStackTrace();
        }

    }
    public String getMessage(){
        try {
            return serverOut.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public void sendMessage(String command){
        try {
            serverIn.writeBytes(command + '\n');
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String msg(String command){
        sendMessage(command);
        return getMessage();
    }
    public void close() {
        try {
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}